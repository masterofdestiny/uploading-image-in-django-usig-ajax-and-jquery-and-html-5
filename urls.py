from django.conf.urls.defaults import patterns, include, url


import settings
from upload.views import showpage ,ajax_upload,secpage
import os
                      
urlpatterns = patterns('',
    url( r'ajax_upload/$', ajax_upload, name="ajax_upload" ),
    url( r'firstpage/$', showpage, name="upload_page" ),
    url( r'secondpage/$', secpage, name="sec_page" ),
)
if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
    (r'^static_media/(?P<path>.*)$', 
        'serve', {
        'document_root': os.path.dirname(__file__)+'/static',
        'show_indexes': True }),)