
from upload.models import Fileupload
from django.template import RequestContext
from django.shortcuts import render_to_response

def showpage(request):
    return render_to_response('upload.html', {}, context_instance=RequestContext(request))

def secpage(request):
    return render_to_response('second.html', {}, context_instance=RequestContext(request))

def ajax_upload(request):
    if request.POST:
        hi = Fileupload()
        hi.image = request.FILES['file']
        hi.save()
    return render_to_response('upload.html', {}, context_instance=RequestContext(request))
        
